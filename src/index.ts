import {TemplatesGrid} from "./views/TemplatesGrid";
import {SettingsForm} from "./views/SettingsForm";

// initialize
webflow.setExtensionSize("comfortable");
TemplatesGrid.show();

// Event Handlers
window.onload = () => {
    const settingsIcon = document.getElementById("settings")!;
    const crossIcon = document.getElementById("cross")!;

    settingsIcon.addEventListener("click", settingsClickHandler);
    crossIcon.addEventListener("click", crossClickHandler);
};

function settingsClickHandler() {
    const settingsIcon = document.getElementById("settings")!;
    const crossIcon = document.getElementById("cross")!;

    settingsIcon.style.display = "none";
    crossIcon.style.display = "block";

    document.getElementById("images-row")!.remove();

    const settingsForm = SettingsForm.createForm();
    document.body.append(settingsForm);
}

function crossClickHandler() {
    const crossIcon = document.getElementById("cross")!;
    const settingsIcon = document.getElementById("settings")!;

    crossIcon.style.display = "none";
    settingsIcon.style.display = "block";

    TemplatesGrid.show();
}
