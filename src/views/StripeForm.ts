import {Commons} from "../utils";
import {Revuflow} from "../api-helpers/Revuflow";
import {CompanyForm} from "./CompanyForm";

export const StripeForm = {
    createForm(templateId: string): void {
        document.getElementById("images-row").remove();

        const stripeForm = document.createElement('form');
        stripeForm.setAttribute("id", "stripe-form");
        stripeForm.innerHTML = `
        <img src="https://storage.googleapis.com/revuflow-assets/revuflow_logo_inversed.png" width="100%" alt="revuflow_logo"/>
        <div style="font-size: 12px">Not suscribed yet ? Visit <a href="https://www.revuflow.net/" target="_blank" style="color: #1280EE">our website</a> to enjoy your 30-day free trial now !</div>
        <br>
        <label style="font-size: 14px" for="stripe-email" class="ts-small">Enter your Stripe subscription e-mail</label>
        <input type="email" id="stripe-email">
        <div style="font-size: 14px" id="matching-emails">
        The e-mail address you enter here must match with the e-mail you used to pay for RevuFlow on Stripe.
        </div>
        <button>Submit</button>
    `;

        document.body.append(stripeForm);
        stripeForm.addEventListener("submit", (event) => {
            const email: string = (document.getElementById("stripe-email") as HTMLInputElement).value;
            processSubscription(email, templateId, event);
        });
    }
}

async function processSubscription(email: string, templateId: string, event?: Event) {
    if (event) event.preventDefault();

    if (!Commons.validateEmail(email)) {
        webflow.notify({type: 'Error', message: 'The e-mail format is invalid.'});
        return;
    }

    const siteInfo = await webflow.getSiteInfo();

    const userHasAccess = await Revuflow.hasAccess(email, siteInfo.siteId);
    const userHasSubscription = await Revuflow.hasSubscription(email);

    if (!userHasSubscription) {
        await webflow.notify({type: 'Error', message: 'The given e-mail does not have any active subscription.'});

        return;
    }
    if (!userHasAccess) {
        const userHasMetadata = await Revuflow.hasMetadata(email);
        if (!userHasMetadata) {
            await Revuflow.updateUserMetadata(email, siteInfo.siteId);

            const companyForm = CompanyForm.createForm();
            const stripeForm = document.getElementById("stripe-form");

            if (stripeForm) stripeForm.replaceWith(companyForm);
        } else {
            webflow.notify({type: 'Error', message: 'The given e-mail does not have access to the extension for this website.'});
        }

        return;
    }

    try {
        if (!localStorage.getItem("revuflow-email")) {
            localStorage.setItem("revuflow-email", email);
        }

        const companyName = localStorage.getItem("revuflow-company");

        if (companyName) {
            await CompanyForm.handleForm(templateId);
        } else if (userHasAccess) {
            const companyForm = CompanyForm.createForm();
            const stripeForm = document.getElementById("stripe-form");

            if (stripeForm) stripeForm.replaceWith(companyForm);

            document.getElementById("company-form")!.addEventListener("submit", event => CompanyForm.handleForm(templateId, event));
        } else {
            StripeForm.createForm(templateId);
        }
    } catch (e) {
        console.error("Failed to fetch subscription status:", e);
    }
}
