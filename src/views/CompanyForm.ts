import {TemplatesGrid} from "./TemplatesGrid";

export const CompanyForm = {
    createForm(): HTMLFormElement
    {
        const form = document.createElement('form');
        form.setAttribute('id', 'company-form');
        form.innerHTML = `
    <img src="https://storage.googleapis.com/revuflow-assets/revuflow_logo_inversed.png" width="100%" alt="revuflow_logo"/>
    <label for="company">Enter your company name</label>
    <input type="text" id="company">
    <button>Submit</button>
    `
        return form;
    },

    async handleForm(templateId: string, event ?: any): Promise<void> {
        if (event) event.preventDefault();

        const companyName = localStorage.getItem("revuflow-company") ?? (document.getElementById("company") as HTMLInputElement).value
        localStorage.setItem("revuflow-company", companyName);

        const section = await webflow.getSelectedElement();

        if (!section) {
            webflow.notify({type: 'Error', message: 'Please select an element before trying to display the widget.'});
        } else if (section.children && section.configurable) {
            try {
                let contentDiv = webflow.createDOM('iframe');
                switch (templateId) {
                    case "2":
                        contentDiv.setAttribute('src', 'https://api.revuflow.net/widgets?template=carousel&company_name=' + companyName);
                        contentDiv.setAttribute('style', [
                            'width: 100%',
                            'border: none',
                            'height: 390px'
                        ].join('; '));
                        break;
                    case "3":
                        contentDiv.setAttribute('src', 'https://api.revuflow.net/widgets?template=badge&company_name=' + companyName);
                        contentDiv.setAttribute('style', [
                            'position: fixed',
                            'left: 0',
                            'bottom: 0',
                            'z-index: 1000',
                            'border: none'
                        ].join('; '));
                        break;
                    default:
                        break;
                }
                section.setChildren([contentDiv]);
                await section.save();

                webflow.notify({type: 'Success', message: 'The widget is displayed on your page !'});

                if (!document.getElementById("images-row")) await TemplatesGrid.show();
            } catch (error) {
                console.error('Une erreur s\'est produite lors de l\'appel à l\'API :', error);
            }
        }
    }
}
