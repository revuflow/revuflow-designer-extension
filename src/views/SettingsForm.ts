import {TemplatesGrid} from "./TemplatesGrid";

export const SettingsForm = {

    createForm(): HTMLFormElement {
        const company = localStorage.getItem("revuflow-company")
        const form = document.createElement('form');
        form.setAttribute('id', 'company-form');
        form.innerHTML = `
    <img src="https://storage.googleapis.com/revuflow-assets/revuflow_logo_inversed.png" width="50%" alt="revuflow_logo"/>
    <br>
    <label for="company">Company name</label>
    <input type="text" id="company" value="${company}">
    <button>Submit</button>
    `

        form.addEventListener("submit", settingsSubmitHandler);

        return form;
    }
}

function settingsSubmitHandler(e: Event) {
    e.preventDefault();

    const companyInput = document.getElementById("company") as HTMLInputElement;
    const company = companyInput.value;

    localStorage.setItem("revuflow-company", company);

    const crossIcon = document.getElementById("cross")!;
    const settingsIcon = document.getElementById("settings")!;

    crossIcon.style.display = "none";
    settingsIcon.style.display = "block";

    TemplatesGrid.show();
}
