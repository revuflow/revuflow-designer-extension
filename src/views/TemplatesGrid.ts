import {Commons} from "../utils";
import {StripeForm} from "./StripeForm";
import {CompanyForm} from "./CompanyForm";

const AVAILABLE_TEMPLATES = [2, 3, 4];

export const TemplatesGrid = {

    async show(): Promise<void> {
        removeElementById("company-form");
        removeElementById("stripe-form");

        const response = await fetch('https://storage.googleapis.com/storage/v1/b/revuflow-assets/o?prefix=templates/');
        const items = (await response.json()).items;
        const templates = items.slice(1);

        const row = createDivWithAttributesAndStyles({ id: "images-row" }, { display: "flex" });
        const leftColumn = createDivWithAttributesAndStyles({}, { flex: "50%", padding: "5px" });
        const rightColumn = createDivWithAttributesAndStyles({}, { flex: "50%", padding: "5px" });

        const isEmailStoredAndValid = Commons.validateEmail(localStorage.getItem("revuflow-email") ?? "");
        const images = createImages(templates, !!isEmailStoredAndValid);

        // create the two templates rows
        appendChildrenToElement(leftColumn, images.slice(0, images.length / 2));
        appendChildrenToElement(rightColumn, images.slice(images.length / 2));

        row.append(leftColumn, rightColumn);

        document.body.append(row);
    }
}

function removeElementById(id: string): void {
    const element = document.getElementById(id);
    if (element) element.remove();
}

function createDivWithAttributesAndStyles(attributes: { [key: string]: string }, styles: { [key: string]: string }): HTMLDivElement {
    const div = document.createElement("div");
    for (const [key, value] of Object.entries(attributes)) {
        div.setAttribute(key, value);
    }
    Object.assign(div.style, styles);
    return div;
}

function createImages(templates: any[], isEmailStoredAndValid: boolean | null): HTMLImageElement[] {
    return templates.map((template, index) => {
        const templateId = `${index + 1}`;
        const image = document.createElement("img");

        image.setAttribute("src", `https://storage.googleapis.com/revuflow-assets/${template.name}`);
        image.setAttribute("template-id", templateId);
        image.setAttribute("width", "100%");

        if (!(AVAILABLE_TEMPLATES.indexOf(index + 1) >= 0)) {
            image.style.filter = "blur(0.15rem)";
        } else {
            image.style.cursor = "pointer";
            image.onclick = createImageClickHandler(templateId, isEmailStoredAndValid);
        }

        return image;
    });
}

function createImageClickHandler(templateId: string, isEmailStoredAndValid: boolean | null): () => void {
    return () => {
        if (!isEmailStoredAndValid) {
            StripeForm.createForm(templateId);
        } else {
            if (!localStorage.getItem("revuflow-company")) {
                const companyForm = CompanyForm.createForm();
                replaceElementById("images-row", companyForm);
                document.getElementById("company-form")!.addEventListener("submit", (event) => CompanyForm.handleForm(templateId, event));
            } else {
                CompanyForm.handleForm(templateId);
            }
        }
    };
}

function replaceElementById(id: string, newElement: HTMLElement): void {
    const oldElement = document.getElementById(id);
    if (oldElement) oldElement.replaceWith(newElement);
}

function appendChildrenToElement(element: HTMLElement, children: HTMLElement[]): void {
    children.forEach(child => element.append(child));
}
