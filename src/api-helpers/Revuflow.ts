const REVUFLOW_URL = 'https://api.revuflow.net';
// const REVUFLOW_URL = 'http://localhost:8080';

export const Revuflow = {
    async hasSubscription(email: string): Promise<boolean> {
        return await fetch(`${REVUFLOW_URL}/stripe/has-subscription?email=${email}`).then(response => response.json()).then((data) => {

            return data.data;
        });
    },

    async hasMetadata(email: string): Promise<boolean> {
        return await fetch(`${REVUFLOW_URL}/stripe/has-metadata?email=${email}`).then(response => response.json()).then((data) => {

            return data.data;
        });
    },

    async hasAccess(email: string, websiteId: string): Promise<boolean> {
        return await fetch(`${REVUFLOW_URL}/stripe/has-access?email=${email}&websiteId=${websiteId}`).then(response => response.json()).then((data) => {

            return data.data;
        });
    },

    async updateUserMetadata(email: string, website_id: string) {

        return await fetch(`${REVUFLOW_URL}/stripe/customers/${email}`, {
            method: "PUT",
            headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify({
                field_to_update: "metadata",
                website_id
            })
        });
    },

    async getReviews(companyName: string): Promise<any> {
        const response = await fetch(`${REVUFLOW_URL}/reviews?companyName=${companyName}&includes=reviews`);

        return await response.json();
    }
}
