/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/api-helpers/Revuflow.ts":
/*!*************************************!*\
  !*** ./src/api-helpers/Revuflow.ts ***!
  \*************************************/
/***/ (function(__unused_webpack_module, exports) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Revuflow = void 0;
var REVUFLOW_URL = 'https://api.revuflow.net';
// const REVUFLOW_URL = 'http://localhost:8080';
exports.Revuflow = {
    hasSubscription: function (email) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch("".concat(REVUFLOW_URL, "/stripe/has-subscription?email=").concat(email)).then(function (response) { return response.json(); }).then(function (data) {
                            return data.data;
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    },
    hasMetadata: function (email) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch("".concat(REVUFLOW_URL, "/stripe/has-metadata?email=").concat(email)).then(function (response) { return response.json(); }).then(function (data) {
                            return data.data;
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    },
    hasAccess: function (email, websiteId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch("".concat(REVUFLOW_URL, "/stripe/has-access?email=").concat(email, "&websiteId=").concat(websiteId)).then(function (response) { return response.json(); }).then(function (data) {
                            return data.data;
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    },
    updateUserMetadata: function (email, website_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch("".concat(REVUFLOW_URL, "/stripe/customers/").concat(email), {
                            method: "PUT",
                            headers: {
                                'content-type': 'application/json;charset=UTF-8',
                            },
                            body: JSON.stringify({
                                field_to_update: "metadata",
                                website_id: website_id
                            })
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    },
    getReviews: function (companyName) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch("".concat(REVUFLOW_URL, "/reviews?companyName=").concat(companyName, "&includes=reviews"))];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.json()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    }
};


/***/ }),

/***/ "./src/utils.ts":
/*!**********************!*\
  !*** ./src/utils.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Commons = void 0;
exports.Commons = {
    validateEmail: function (email) {
        var emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return email ? email.toLowerCase().match(emailRegex) : null;
    }
};


/***/ }),

/***/ "./src/views/CompanyForm.ts":
/*!**********************************!*\
  !*** ./src/views/CompanyForm.ts ***!
  \**********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CompanyForm = void 0;
var TemplatesGrid_1 = __webpack_require__(/*! ./TemplatesGrid */ "./src/views/TemplatesGrid.ts");
exports.CompanyForm = {
    createForm: function () {
        var form = document.createElement('form');
        form.setAttribute('id', 'company-form');
        form.innerHTML = "\n    <img src=\"https://storage.googleapis.com/revuflow-assets/revuflow_logo_inversed.png\" width=\"100%\" alt=\"revuflow_logo\"/>\n    <label for=\"company\">Enter your company name</label>\n    <input type=\"text\" id=\"company\">\n    <button>Submit</button>\n    ";
        return form;
    },
    handleForm: function (templateId, event) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var companyName, section, contentDiv, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (event)
                            event.preventDefault();
                        companyName = (_a = localStorage.getItem("revuflow-company")) !== null && _a !== void 0 ? _a : document.getElementById("company").value;
                        localStorage.setItem("revuflow-company", companyName);
                        return [4 /*yield*/, webflow.getSelectedElement()];
                    case 1:
                        section = _b.sent();
                        if (!!section) return [3 /*break*/, 2];
                        webflow.notify({ type: 'Error', message: 'Please select an element before trying to display the widget.' });
                        return [3 /*break*/, 8];
                    case 2:
                        if (!(section.children && section.configurable)) return [3 /*break*/, 8];
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 7, , 8]);
                        contentDiv = webflow.createDOM('iframe');
                        switch (templateId) {
                            case "2":
                                contentDiv.setAttribute('src', 'https://api.revuflow.net/widgets?template=carousel&company_name=' + companyName);
                                contentDiv.setAttribute('style', [
                                    'width: 100%',
                                    'border: none',
                                    'height: 390px'
                                ].join('; '));
                                break;
                            case "3":
                                contentDiv.setAttribute('src', 'https://api.revuflow.net/widgets?template=badge&company_name=' + companyName);
                                contentDiv.setAttribute('style', [
                                    'position: fixed',
                                    'left: 0',
                                    'bottom: 0',
                                    'z-index: 1000',
                                    'border: none'
                                ].join('; '));
                                break;
                            default:
                                break;
                        }
                        section.setChildren([contentDiv]);
                        return [4 /*yield*/, section.save()];
                    case 4:
                        _b.sent();
                        webflow.notify({ type: 'Success', message: 'The widget is displayed on your page !' });
                        if (!!document.getElementById("images-row")) return [3 /*break*/, 6];
                        return [4 /*yield*/, TemplatesGrid_1.TemplatesGrid.show()];
                    case 5:
                        _b.sent();
                        _b.label = 6;
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        error_1 = _b.sent();
                        console.error('Une erreur s\'est produite lors de l\'appel à l\'API :', error_1);
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/];
                }
            });
        });
    }
};


/***/ }),

/***/ "./src/views/SettingsForm.ts":
/*!***********************************!*\
  !*** ./src/views/SettingsForm.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SettingsForm = void 0;
var TemplatesGrid_1 = __webpack_require__(/*! ./TemplatesGrid */ "./src/views/TemplatesGrid.ts");
exports.SettingsForm = {
    createForm: function () {
        var company = localStorage.getItem("revuflow-company");
        var form = document.createElement('form');
        form.setAttribute('id', 'company-form');
        form.innerHTML = "\n    <img src=\"https://storage.googleapis.com/revuflow-assets/revuflow_logo_inversed.png\" width=\"50%\" alt=\"revuflow_logo\"/>\n    <br>\n    <label for=\"company\">Company name</label>\n    <input type=\"text\" id=\"company\" value=\"".concat(company, "\">\n    <button>Submit</button>\n    ");
        form.addEventListener("submit", settingsSubmitHandler);
        return form;
    }
};
function settingsSubmitHandler(e) {
    e.preventDefault();
    var companyInput = document.getElementById("company");
    var company = companyInput.value;
    localStorage.setItem("revuflow-company", company);
    var crossIcon = document.getElementById("cross");
    var settingsIcon = document.getElementById("settings");
    crossIcon.style.display = "none";
    settingsIcon.style.display = "block";
    TemplatesGrid_1.TemplatesGrid.show();
}


/***/ }),

/***/ "./src/views/StripeForm.ts":
/*!*********************************!*\
  !*** ./src/views/StripeForm.ts ***!
  \*********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.StripeForm = void 0;
var utils_1 = __webpack_require__(/*! ../utils */ "./src/utils.ts");
var Revuflow_1 = __webpack_require__(/*! ../api-helpers/Revuflow */ "./src/api-helpers/Revuflow.ts");
var CompanyForm_1 = __webpack_require__(/*! ./CompanyForm */ "./src/views/CompanyForm.ts");
exports.StripeForm = {
    createForm: function (templateId) {
        document.getElementById("images-row").remove();
        var stripeForm = document.createElement('form');
        stripeForm.setAttribute("id", "stripe-form");
        stripeForm.innerHTML = "\n        <img src=\"https://storage.googleapis.com/revuflow-assets/revuflow_logo_inversed.png\" width=\"100%\" alt=\"revuflow_logo\"/>\n        <div style=\"font-size: 12px\">Not suscribed yet ? Visit <a href=\"https://www.revuflow.net/\" target=\"_blank\" style=\"color: #1280EE\">our website</a> to enjoy your 30-day free trial now !</div>\n        <br>\n        <label style=\"font-size: 14px\" for=\"stripe-email\" class=\"ts-small\">Enter your Stripe subscription e-mail</label>\n        <input type=\"email\" id=\"stripe-email\">\n        <div style=\"font-size: 14px\" id=\"matching-emails\">\n        The e-mail address you enter here must match with the e-mail you used to pay for RevuFlow on Stripe.\n        </div>\n        <button>Submit</button>\n    ";
        document.body.append(stripeForm);
        stripeForm.addEventListener("submit", function (event) {
            var email = document.getElementById("stripe-email").value;
            processSubscription(email, templateId, event);
        });
    }
};
function processSubscription(email, templateId, event) {
    return __awaiter(this, void 0, void 0, function () {
        var siteInfo, userHasAccess, userHasSubscription, userHasMetadata, companyForm, stripeForm, companyName, companyForm, stripeForm, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (event)
                        event.preventDefault();
                    if (!utils_1.Commons.validateEmail(email)) {
                        webflow.notify({ type: 'Error', message: 'The e-mail format is invalid.' });
                        return [2 /*return*/];
                    }
                    return [4 /*yield*/, webflow.getSiteInfo()];
                case 1:
                    siteInfo = _a.sent();
                    return [4 /*yield*/, Revuflow_1.Revuflow.hasAccess(email, siteInfo.siteId)];
                case 2:
                    userHasAccess = _a.sent();
                    return [4 /*yield*/, Revuflow_1.Revuflow.hasSubscription(email)];
                case 3:
                    userHasSubscription = _a.sent();
                    if (!!userHasSubscription) return [3 /*break*/, 5];
                    return [4 /*yield*/, webflow.notify({ type: 'Error', message: 'The given e-mail does not have any active subscription.' })];
                case 4:
                    _a.sent();
                    return [2 /*return*/];
                case 5:
                    if (!!userHasAccess) return [3 /*break*/, 10];
                    return [4 /*yield*/, Revuflow_1.Revuflow.hasMetadata(email)];
                case 6:
                    userHasMetadata = _a.sent();
                    if (!!userHasMetadata) return [3 /*break*/, 8];
                    return [4 /*yield*/, Revuflow_1.Revuflow.updateUserMetadata(email, siteInfo.siteId)];
                case 7:
                    _a.sent();
                    companyForm = CompanyForm_1.CompanyForm.createForm();
                    stripeForm = document.getElementById("stripe-form");
                    if (stripeForm)
                        stripeForm.replaceWith(companyForm);
                    return [3 /*break*/, 9];
                case 8:
                    webflow.notify({ type: 'Error', message: 'The given e-mail does not have access to the extension for this website.' });
                    _a.label = 9;
                case 9: return [2 /*return*/];
                case 10:
                    _a.trys.push([10, 14, , 15]);
                    if (!localStorage.getItem("revuflow-email")) {
                        localStorage.setItem("revuflow-email", email);
                    }
                    companyName = localStorage.getItem("revuflow-company");
                    if (!companyName) return [3 /*break*/, 12];
                    return [4 /*yield*/, CompanyForm_1.CompanyForm.handleForm(templateId)];
                case 11:
                    _a.sent();
                    return [3 /*break*/, 13];
                case 12:
                    if (userHasAccess) {
                        companyForm = CompanyForm_1.CompanyForm.createForm();
                        stripeForm = document.getElementById("stripe-form");
                        if (stripeForm)
                            stripeForm.replaceWith(companyForm);
                        document.getElementById("company-form").addEventListener("submit", function (event) { return CompanyForm_1.CompanyForm.handleForm(templateId, event); });
                    }
                    else {
                        exports.StripeForm.createForm(templateId);
                    }
                    _a.label = 13;
                case 13: return [3 /*break*/, 15];
                case 14:
                    e_1 = _a.sent();
                    console.error("Failed to fetch subscription status:", e_1);
                    return [3 /*break*/, 15];
                case 15: return [2 /*return*/];
            }
        });
    });
}


/***/ }),

/***/ "./src/views/TemplatesGrid.ts":
/*!************************************!*\
  !*** ./src/views/TemplatesGrid.ts ***!
  \************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.TemplatesGrid = void 0;
var utils_1 = __webpack_require__(/*! ../utils */ "./src/utils.ts");
var StripeForm_1 = __webpack_require__(/*! ./StripeForm */ "./src/views/StripeForm.ts");
var CompanyForm_1 = __webpack_require__(/*! ./CompanyForm */ "./src/views/CompanyForm.ts");
var AVAILABLE_TEMPLATES = [2, 3];
exports.TemplatesGrid = {
    show: function () {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var response, items, templates, row, leftColumn, rightColumn, isEmailStoredAndValid, images;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        removeElementById("company-form");
                        removeElementById("stripe-form");
                        return [4 /*yield*/, fetch('https://storage.googleapis.com/storage/v1/b/revuflow-assets/o?prefix=templates/')];
                    case 1:
                        response = _b.sent();
                        return [4 /*yield*/, response.json()];
                    case 2:
                        items = (_b.sent()).items;
                        templates = items.slice(1);
                        row = createDivWithAttributesAndStyles({ id: "images-row" }, { display: "flex" });
                        leftColumn = createDivWithAttributesAndStyles({}, { flex: "50%", padding: "5px" });
                        rightColumn = createDivWithAttributesAndStyles({}, { flex: "50%", padding: "5px" });
                        isEmailStoredAndValid = utils_1.Commons.validateEmail((_a = localStorage.getItem("revuflow-email")) !== null && _a !== void 0 ? _a : "");
                        images = createImages(templates, !!isEmailStoredAndValid);
                        // create the two templates rows
                        appendChildrenToElement(leftColumn, images.slice(0, images.length / 2));
                        appendChildrenToElement(rightColumn, images.slice(images.length / 2));
                        row.append(leftColumn, rightColumn);
                        document.body.append(row);
                        return [2 /*return*/];
                }
            });
        });
    }
};
function removeElementById(id) {
    var element = document.getElementById(id);
    if (element)
        element.remove();
}
function createDivWithAttributesAndStyles(attributes, styles) {
    var div = document.createElement("div");
    for (var _i = 0, _a = Object.entries(attributes); _i < _a.length; _i++) {
        var _b = _a[_i], key = _b[0], value = _b[1];
        div.setAttribute(key, value);
    }
    Object.assign(div.style, styles);
    return div;
}
function createImages(templates, isEmailStoredAndValid) {
    return templates.map(function (template, index) {
        var templateId = "".concat(index + 1);
        var image = document.createElement("img");
        image.setAttribute("src", "https://storage.googleapis.com/revuflow-assets/".concat(template.name));
        image.setAttribute("template-id", templateId);
        image.setAttribute("width", "100%");
        if (!(AVAILABLE_TEMPLATES.indexOf(index + 1) >= 0)) {
            image.style.filter = "blur(0.15rem)";
        }
        else {
            image.style.cursor = "pointer";
            image.onclick = createImageClickHandler(templateId, isEmailStoredAndValid);
        }
        return image;
    });
}
function createImageClickHandler(templateId, isEmailStoredAndValid) {
    return function () {
        if (!isEmailStoredAndValid) {
            StripeForm_1.StripeForm.createForm(templateId);
        }
        else {
            if (!localStorage.getItem("revuflow-company")) {
                var companyForm = CompanyForm_1.CompanyForm.createForm();
                replaceElementById("images-row", companyForm);
                document.getElementById("company-form").addEventListener("submit", function (event) { return CompanyForm_1.CompanyForm.handleForm(templateId, event); });
            }
            else {
                CompanyForm_1.CompanyForm.handleForm(templateId);
            }
        }
    };
}
function replaceElementById(id, newElement) {
    var oldElement = document.getElementById(id);
    if (oldElement)
        oldElement.replaceWith(newElement);
}
function appendChildrenToElement(element, children) {
    children.forEach(function (child) { return element.append(child); });
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
var TemplatesGrid_1 = __webpack_require__(/*! ./views/TemplatesGrid */ "./src/views/TemplatesGrid.ts");
var SettingsForm_1 = __webpack_require__(/*! ./views/SettingsForm */ "./src/views/SettingsForm.ts");
// initialize
webflow.setExtensionSize("comfortable");
TemplatesGrid_1.TemplatesGrid.show();
// Event Handlers
window.onload = function () {
    var settingsIcon = document.getElementById("settings");
    var crossIcon = document.getElementById("cross");
    settingsIcon.addEventListener("click", settingsClickHandler);
    crossIcon.addEventListener("click", crossClickHandler);
};
function settingsClickHandler() {
    var settingsIcon = document.getElementById("settings");
    var crossIcon = document.getElementById("cross");
    settingsIcon.style.display = "none";
    crossIcon.style.display = "block";
    document.getElementById("images-row").remove();
    var settingsForm = SettingsForm_1.SettingsForm.createForm();
    document.body.append(settingsForm);
}
function crossClickHandler() {
    var crossIcon = document.getElementById("cross");
    var settingsIcon = document.getElementById("settings");
    crossIcon.style.display = "none";
    settingsIcon.style.display = "block";
    TemplatesGrid_1.TemplatesGrid.show();
}

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFhO0FBQ2I7QUFDQSw0QkFBNEIsK0RBQStELGlCQUFpQjtBQUM1RztBQUNBLG9DQUFvQyxNQUFNLCtCQUErQixZQUFZO0FBQ3JGLG1DQUFtQyxNQUFNLG1DQUFtQyxZQUFZO0FBQ3hGLGdDQUFnQztBQUNoQztBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsY0FBYyw2QkFBNkIsMEJBQTBCLGNBQWMscUJBQXFCO0FBQ3hHLGlCQUFpQixvREFBb0QscUVBQXFFLGNBQWM7QUFDeEosdUJBQXVCLHNCQUFzQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0M7QUFDeEMsbUNBQW1DLFNBQVM7QUFDNUMsbUNBQW1DLFdBQVcsVUFBVTtBQUN4RCwwQ0FBMEMsY0FBYztBQUN4RDtBQUNBLDhHQUE4RyxPQUFPO0FBQ3JILGlGQUFpRixpQkFBaUI7QUFDbEcseURBQXlELGdCQUFnQixRQUFRO0FBQ2pGLCtDQUErQyxnQkFBZ0IsZ0JBQWdCO0FBQy9FO0FBQ0Esa0NBQWtDO0FBQ2xDO0FBQ0E7QUFDQSxVQUFVLFlBQVksYUFBYSxTQUFTLFVBQVU7QUFDdEQsb0NBQW9DLFNBQVM7QUFDN0M7QUFDQTtBQUNBLDhDQUE2QyxFQUFFLGFBQWEsRUFBQztBQUM3RCxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRKQUE0Six5QkFBeUI7QUFDckw7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0pBQXdKLHlCQUF5QjtBQUNqTDtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1TEFBdUwseUJBQXlCO0FBQ2hOO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtFQUFrRTtBQUNsRSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTs7Ozs7Ozs7Ozs7QUMvR2E7QUFDYiw4Q0FBNkMsRUFBRSxhQUFhLEVBQUM7QUFDN0QsZUFBZTtBQUNmLGVBQWU7QUFDZjtBQUNBLDJDQUEyQyx3QkFBd0IsOEJBQThCLElBQUksUUFBUSxJQUFJLFFBQVEsSUFBSSxRQUFRLElBQUksaUNBQWlDLEdBQUc7QUFDN0s7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1JhO0FBQ2I7QUFDQSw0QkFBNEIsK0RBQStELGlCQUFpQjtBQUM1RztBQUNBLG9DQUFvQyxNQUFNLCtCQUErQixZQUFZO0FBQ3JGLG1DQUFtQyxNQUFNLG1DQUFtQyxZQUFZO0FBQ3hGLGdDQUFnQztBQUNoQztBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsY0FBYyw2QkFBNkIsMEJBQTBCLGNBQWMscUJBQXFCO0FBQ3hHLGlCQUFpQixvREFBb0QscUVBQXFFLGNBQWM7QUFDeEosdUJBQXVCLHNCQUFzQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0M7QUFDeEMsbUNBQW1DLFNBQVM7QUFDNUMsbUNBQW1DLFdBQVcsVUFBVTtBQUN4RCwwQ0FBMEMsY0FBYztBQUN4RDtBQUNBLDhHQUE4RyxPQUFPO0FBQ3JILGlGQUFpRixpQkFBaUI7QUFDbEcseURBQXlELGdCQUFnQixRQUFRO0FBQ2pGLCtDQUErQyxnQkFBZ0IsZ0JBQWdCO0FBQy9FO0FBQ0Esa0NBQWtDO0FBQ2xDO0FBQ0E7QUFDQSxVQUFVLFlBQVksYUFBYSxTQUFTLFVBQVU7QUFDdEQsb0NBQW9DLFNBQVM7QUFDN0M7QUFDQTtBQUNBLDhDQUE2QyxFQUFFLGFBQWEsRUFBQztBQUM3RCxtQkFBbUI7QUFDbkIsc0JBQXNCLG1CQUFPLENBQUMscURBQWlCO0FBQy9DLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5Qyx5RkFBeUY7QUFDbEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBDQUEwQztBQUMxQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEM7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QyxvRUFBb0U7QUFDN0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBOzs7Ozs7Ozs7OztBQ2hIYTtBQUNiLDhDQUE2QyxFQUFFLGFBQWEsRUFBQztBQUM3RCxvQkFBb0I7QUFDcEIsc0JBQXNCLG1CQUFPLENBQUMscURBQWlCO0FBQy9DLG9CQUFvQjtBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ3hCYTtBQUNiO0FBQ0EsNEJBQTRCLCtEQUErRCxpQkFBaUI7QUFDNUc7QUFDQSxvQ0FBb0MsTUFBTSwrQkFBK0IsWUFBWTtBQUNyRixtQ0FBbUMsTUFBTSxtQ0FBbUMsWUFBWTtBQUN4RixnQ0FBZ0M7QUFDaEM7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLGNBQWMsNkJBQTZCLDBCQUEwQixjQUFjLHFCQUFxQjtBQUN4RyxpQkFBaUIsb0RBQW9ELHFFQUFxRSxjQUFjO0FBQ3hKLHVCQUF1QixzQkFBc0I7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDO0FBQ3hDLG1DQUFtQyxTQUFTO0FBQzVDLG1DQUFtQyxXQUFXLFVBQVU7QUFDeEQsMENBQTBDLGNBQWM7QUFDeEQ7QUFDQSw4R0FBOEcsT0FBTztBQUNySCxpRkFBaUYsaUJBQWlCO0FBQ2xHLHlEQUF5RCxnQkFBZ0IsUUFBUTtBQUNqRiwrQ0FBK0MsZ0JBQWdCLGdCQUFnQjtBQUMvRTtBQUNBLGtDQUFrQztBQUNsQztBQUNBO0FBQ0EsVUFBVSxZQUFZLGFBQWEsU0FBUyxVQUFVO0FBQ3RELG9DQUFvQyxTQUFTO0FBQzdDO0FBQ0E7QUFDQSw4Q0FBNkMsRUFBRSxhQUFhLEVBQUM7QUFDN0Qsa0JBQWtCO0FBQ2xCLGNBQWMsbUJBQU8sQ0FBQyxnQ0FBVTtBQUNoQyxpQkFBaUIsbUJBQU8sQ0FBQyw4REFBeUI7QUFDbEQsb0JBQW9CLG1CQUFPLENBQUMsaURBQWU7QUFDM0Msa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDLHlEQUF5RDtBQUNsRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwREFBMEQsbUZBQW1GO0FBQzdJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyxvR0FBb0c7QUFDekk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4R0FBOEcsaUVBQWlFO0FBQy9LO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7Ozs7Ozs7Ozs7O0FDbklhO0FBQ2I7QUFDQSw0QkFBNEIsK0RBQStELGlCQUFpQjtBQUM1RztBQUNBLG9DQUFvQyxNQUFNLCtCQUErQixZQUFZO0FBQ3JGLG1DQUFtQyxNQUFNLG1DQUFtQyxZQUFZO0FBQ3hGLGdDQUFnQztBQUNoQztBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsY0FBYyw2QkFBNkIsMEJBQTBCLGNBQWMscUJBQXFCO0FBQ3hHLGlCQUFpQixvREFBb0QscUVBQXFFLGNBQWM7QUFDeEosdUJBQXVCLHNCQUFzQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0M7QUFDeEMsbUNBQW1DLFNBQVM7QUFDNUMsbUNBQW1DLFdBQVcsVUFBVTtBQUN4RCwwQ0FBMEMsY0FBYztBQUN4RDtBQUNBLDhHQUE4RyxPQUFPO0FBQ3JILGlGQUFpRixpQkFBaUI7QUFDbEcseURBQXlELGdCQUFnQixRQUFRO0FBQ2pGLCtDQUErQyxnQkFBZ0IsZ0JBQWdCO0FBQy9FO0FBQ0Esa0NBQWtDO0FBQ2xDO0FBQ0E7QUFDQSxVQUFVLFlBQVksYUFBYSxTQUFTLFVBQVU7QUFDdEQsb0NBQW9DLFNBQVM7QUFDN0M7QUFDQTtBQUNBLDhDQUE2QyxFQUFFLGFBQWEsRUFBQztBQUM3RCxxQkFBcUI7QUFDckIsY0FBYyxtQkFBTyxDQUFDLGdDQUFVO0FBQ2hDLG1CQUFtQixtQkFBTyxDQUFDLCtDQUFjO0FBQ3pDLG9CQUFvQixtQkFBTyxDQUFDLGlEQUFlO0FBQzNDO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFLGtCQUFrQixJQUFJLGlCQUFpQjtBQUN4Ryx3RUFBd0UsSUFBSSw2QkFBNkI7QUFDekcseUVBQXlFLElBQUksNkJBQTZCO0FBQzFHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0RBQXNELGdCQUFnQjtBQUN0RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzR0FBc0csaUVBQWlFO0FBQ3ZLO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDLCtCQUErQjtBQUN2RTs7Ozs7OztVQ25JQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7Ozs7Ozs7O0FDdEJhO0FBQ2IsOENBQTZDLEVBQUUsYUFBYSxFQUFDO0FBQzdELHNCQUFzQixtQkFBTyxDQUFDLDJEQUF1QjtBQUNyRCxxQkFBcUIsbUJBQU8sQ0FBQyx5REFBc0I7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZXMiOlsid2VicGFjazovL2dvb2dsZXJldmlld3MvLi9zcmMvYXBpLWhlbHBlcnMvUmV2dWZsb3cudHMiLCJ3ZWJwYWNrOi8vZ29vZ2xlcmV2aWV3cy8uL3NyYy91dGlscy50cyIsIndlYnBhY2s6Ly9nb29nbGVyZXZpZXdzLy4vc3JjL3ZpZXdzL0NvbXBhbnlGb3JtLnRzIiwid2VicGFjazovL2dvb2dsZXJldmlld3MvLi9zcmMvdmlld3MvU2V0dGluZ3NGb3JtLnRzIiwid2VicGFjazovL2dvb2dsZXJldmlld3MvLi9zcmMvdmlld3MvU3RyaXBlRm9ybS50cyIsIndlYnBhY2s6Ly9nb29nbGVyZXZpZXdzLy4vc3JjL3ZpZXdzL1RlbXBsYXRlc0dyaWQudHMiLCJ3ZWJwYWNrOi8vZ29vZ2xlcmV2aWV3cy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9nb29nbGVyZXZpZXdzLy4vc3JjL2luZGV4LnRzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xudmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XG4gICAgZnVuY3Rpb24gYWRvcHQodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUCA/IHZhbHVlIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogYWRvcHQocmVzdWx0LnZhbHVlKS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcbiAgICB9KTtcbn07XG52YXIgX19nZW5lcmF0b3IgPSAodGhpcyAmJiB0aGlzLl9fZ2VuZXJhdG9yKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgYm9keSkge1xuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XG4gICAgICAgIHdoaWxlIChnICYmIChnID0gMCwgb3BbMF0gJiYgKF8gPSAwKSksIF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xuZXhwb3J0cy5SZXZ1ZmxvdyA9IHZvaWQgMDtcbnZhciBSRVZVRkxPV19VUkwgPSAnaHR0cHM6Ly9hcGkucmV2dWZsb3cubmV0Jztcbi8vIGNvbnN0IFJFVlVGTE9XX1VSTCA9ICdodHRwOi8vbG9jYWxob3N0OjgwODAnO1xuZXhwb3J0cy5SZXZ1ZmxvdyA9IHtcbiAgICBoYXNTdWJzY3JpcHRpb246IGZ1bmN0aW9uIChlbWFpbCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDA6IHJldHVybiBbNCAvKnlpZWxkKi8sIGZldGNoKFwiXCIuY29uY2F0KFJFVlVGTE9XX1VSTCwgXCIvc3RyaXBlL2hhcy1zdWJzY3JpcHRpb24/ZW1haWw9XCIpLmNvbmNhdChlbWFpbCkpLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7IHJldHVybiByZXNwb25zZS5qc29uKCk7IH0pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZGF0YS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9LFxuICAgIGhhc01ldGFkYXRhOiBmdW5jdGlvbiAoZW1haWwpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIF9fZ2VuZXJhdG9yKHRoaXMsIGZ1bmN0aW9uIChfYSkge1xuICAgICAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAwOiByZXR1cm4gWzQgLyp5aWVsZCovLCBmZXRjaChcIlwiLmNvbmNhdChSRVZVRkxPV19VUkwsIFwiL3N0cmlwZS9oYXMtbWV0YWRhdGE/ZW1haWw9XCIpLmNvbmNhdChlbWFpbCkpLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7IHJldHVybiByZXNwb25zZS5qc29uKCk7IH0pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZGF0YS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9LFxuICAgIGhhc0FjY2VzczogZnVuY3Rpb24gKGVtYWlsLCB3ZWJzaXRlSWQpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIF9fZ2VuZXJhdG9yKHRoaXMsIGZ1bmN0aW9uIChfYSkge1xuICAgICAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAwOiByZXR1cm4gWzQgLyp5aWVsZCovLCBmZXRjaChcIlwiLmNvbmNhdChSRVZVRkxPV19VUkwsIFwiL3N0cmlwZS9oYXMtYWNjZXNzP2VtYWlsPVwiKS5jb25jYXQoZW1haWwsIFwiJndlYnNpdGVJZD1cIikuY29uY2F0KHdlYnNpdGVJZCkpLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7IHJldHVybiByZXNwb25zZS5qc29uKCk7IH0pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZGF0YS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9LFxuICAgIHVwZGF0ZVVzZXJNZXRhZGF0YTogZnVuY3Rpb24gKGVtYWlsLCB3ZWJzaXRlX2lkKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgZmV0Y2goXCJcIi5jb25jYXQoUkVWVUZMT1dfVVJMLCBcIi9zdHJpcGUvY3VzdG9tZXJzL1wiKS5jb25jYXQoZW1haWwpLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBVVFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2NvbnRlbnQtdHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uO2NoYXJzZXQ9VVRGLTgnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWVsZF90b191cGRhdGU6IFwibWV0YWRhdGFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2Vic2l0ZV9pZDogd2Vic2l0ZV9pZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICB9KV07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIFsyIC8qcmV0dXJuKi8sIF9hLnNlbnQoKV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH0sXG4gICAgZ2V0UmV2aWV3czogZnVuY3Rpb24gKGNvbXBhbnlOYW1lKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciByZXNwb25zZTtcbiAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgZmV0Y2goXCJcIi5jb25jYXQoUkVWVUZMT1dfVVJMLCBcIi9yZXZpZXdzP2NvbXBhbnlOYW1lPVwiKS5jb25jYXQoY29tcGFueU5hbWUsIFwiJmluY2x1ZGVzPXJldmlld3NcIikpXTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgPSBfYS5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCByZXNwb25zZS5qc29uKCldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDI6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59O1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG5leHBvcnRzLkNvbW1vbnMgPSB2b2lkIDA7XG5leHBvcnRzLkNvbW1vbnMgPSB7XG4gICAgdmFsaWRhdGVFbWFpbDogZnVuY3Rpb24gKGVtYWlsKSB7XG4gICAgICAgIHZhciBlbWFpbFJlZ2V4ID0gL14oKFtePD4oKVtcXF1cXFxcLiw7Olxcc0BcIl0rKFxcLltePD4oKVtcXF1cXFxcLiw7Olxcc0BcIl0rKSopfC4oXCIuK1wiKSlAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFxdKXwoKFthLXpBLVpcXC0wLTldK1xcLikrW2EtekEtWl17Mix9KSkkLztcbiAgICAgICAgcmV0dXJuIGVtYWlsID8gZW1haWwudG9Mb3dlckNhc2UoKS5tYXRjaChlbWFpbFJlZ2V4KSA6IG51bGw7XG4gICAgfVxufTtcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XG4gICAgZnVuY3Rpb24gYWRvcHQodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUCA/IHZhbHVlIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogYWRvcHQocmVzdWx0LnZhbHVlKS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcbiAgICB9KTtcbn07XG52YXIgX19nZW5lcmF0b3IgPSAodGhpcyAmJiB0aGlzLl9fZ2VuZXJhdG9yKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgYm9keSkge1xuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XG4gICAgICAgIHdoaWxlIChnICYmIChnID0gMCwgb3BbMF0gJiYgKF8gPSAwKSksIF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xuZXhwb3J0cy5Db21wYW55Rm9ybSA9IHZvaWQgMDtcbnZhciBUZW1wbGF0ZXNHcmlkXzEgPSByZXF1aXJlKFwiLi9UZW1wbGF0ZXNHcmlkXCIpO1xuZXhwb3J0cy5Db21wYW55Rm9ybSA9IHtcbiAgICBjcmVhdGVGb3JtOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBmb3JtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZm9ybScpO1xuICAgICAgICBmb3JtLnNldEF0dHJpYnV0ZSgnaWQnLCAnY29tcGFueS1mb3JtJyk7XG4gICAgICAgIGZvcm0uaW5uZXJIVE1MID0gXCJcXG4gICAgPGltZyBzcmM9XFxcImh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9yZXZ1Zmxvdy1hc3NldHMvcmV2dWZsb3dfbG9nb19pbnZlcnNlZC5wbmdcXFwiIHdpZHRoPVxcXCIxMDAlXFxcIiBhbHQ9XFxcInJldnVmbG93X2xvZ29cXFwiLz5cXG4gICAgPGxhYmVsIGZvcj1cXFwiY29tcGFueVxcXCI+RW50ZXIgeW91ciBjb21wYW55IG5hbWU8L2xhYmVsPlxcbiAgICA8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgaWQ9XFxcImNvbXBhbnlcXFwiPlxcbiAgICA8YnV0dG9uPlN1Ym1pdDwvYnV0dG9uPlxcbiAgICBcIjtcbiAgICAgICAgcmV0dXJuIGZvcm07XG4gICAgfSxcbiAgICBoYW5kbGVGb3JtOiBmdW5jdGlvbiAodGVtcGxhdGVJZCwgZXZlbnQpIHtcbiAgICAgICAgdmFyIF9hO1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgY29tcGFueU5hbWUsIHNlY3Rpb24sIGNvbnRlbnREaXYsIGVycm9yXzE7XG4gICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9iKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChfYi5sYWJlbCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDA6XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBhbnlOYW1lID0gKF9hID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJyZXZ1Zmxvdy1jb21wYW55XCIpKSAhPT0gbnVsbCAmJiBfYSAhPT0gdm9pZCAwID8gX2EgOiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvbXBhbnlcIikudmFsdWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJldnVmbG93LWNvbXBhbnlcIiwgY29tcGFueU5hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgd2ViZmxvdy5nZXRTZWxlY3RlZEVsZW1lbnQoKV07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlY3Rpb24gPSBfYi5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoISFzZWN0aW9uKSByZXR1cm4gWzMgLypicmVhayovLCAyXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdlYmZsb3cubm90aWZ5KHsgdHlwZTogJ0Vycm9yJywgbWVzc2FnZTogJ1BsZWFzZSBzZWxlY3QgYW4gZWxlbWVudCBiZWZvcmUgdHJ5aW5nIHRvIGRpc3BsYXkgdGhlIHdpZGdldC4nIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFszIC8qYnJlYWsqLywgOF07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMjpcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghKHNlY3Rpb24uY2hpbGRyZW4gJiYgc2VjdGlvbi5jb25maWd1cmFibGUpKSByZXR1cm4gWzMgLypicmVhayovLCA4XTtcbiAgICAgICAgICAgICAgICAgICAgICAgIF9iLmxhYmVsID0gMztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAzOlxuICAgICAgICAgICAgICAgICAgICAgICAgX2IudHJ5cy5wdXNoKFszLCA3LCAsIDhdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnREaXYgPSB3ZWJmbG93LmNyZWF0ZURPTSgnaWZyYW1lJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKHRlbXBsYXRlSWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlIFwiMlwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50RGl2LnNldEF0dHJpYnV0ZSgnc3JjJywgJ2h0dHBzOi8vYXBpLnJldnVmbG93Lm5ldC93aWRnZXRzP3RlbXBsYXRlPWNhcm91c2VsJmNvbXBhbnlfbmFtZT0nICsgY29tcGFueU5hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50RGl2LnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnd2lkdGg6IDEwMCUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvcmRlcjogbm9uZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnaGVpZ2h0OiAzOTBweCdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXS5qb2luKCc7ICcpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSBcIjNcIjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudERpdi5zZXRBdHRyaWJ1dGUoJ3NyYycsICdodHRwczovL2FwaS5yZXZ1Zmxvdy5uZXQvd2lkZ2V0cz90ZW1wbGF0ZT1iYWRnZSZjb21wYW55X25hbWU9JyArIGNvbXBhbnlOYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudERpdi5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3Bvc2l0aW9uOiBmaXhlZCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnbGVmdDogMCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm90dG9tOiAwJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICd6LWluZGV4OiAxMDAwJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib3JkZXI6IG5vbmUnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0uam9pbignOyAnKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgc2VjdGlvbi5zZXRDaGlsZHJlbihbY29udGVudERpdl0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgc2VjdGlvbi5zYXZlKCldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgICAgICAgICAgICAgICBfYi5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB3ZWJmbG93Lm5vdGlmeSh7IHR5cGU6ICdTdWNjZXNzJywgbWVzc2FnZTogJ1RoZSB3aWRnZXQgaXMgZGlzcGxheWVkIG9uIHlvdXIgcGFnZSAhJyB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghIWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW1hZ2VzLXJvd1wiKSkgcmV0dXJuIFszIC8qYnJlYWsqLywgNl07XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBUZW1wbGF0ZXNHcmlkXzEuVGVtcGxhdGVzR3JpZC5zaG93KCldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDU6XG4gICAgICAgICAgICAgICAgICAgICAgICBfYi5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBfYi5sYWJlbCA9IDY7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNjogcmV0dXJuIFszIC8qYnJlYWsqLywgOF07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yXzEgPSBfYi5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdVbmUgZXJyZXVyIHNcXCdlc3QgcHJvZHVpdGUgbG9ycyBkZSBsXFwnYXBwZWwgw6AgbFxcJ0FQSSA6JywgZXJyb3JfMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzMgLypicmVhayovLCA4XTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSA4OiByZXR1cm4gWzIgLypyZXR1cm4qL107XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn07XG4iLCJcInVzZSBzdHJpY3RcIjtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcbmV4cG9ydHMuU2V0dGluZ3NGb3JtID0gdm9pZCAwO1xudmFyIFRlbXBsYXRlc0dyaWRfMSA9IHJlcXVpcmUoXCIuL1RlbXBsYXRlc0dyaWRcIik7XG5leHBvcnRzLlNldHRpbmdzRm9ybSA9IHtcbiAgICBjcmVhdGVGb3JtOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBjb21wYW55ID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJyZXZ1Zmxvdy1jb21wYW55XCIpO1xuICAgICAgICB2YXIgZm9ybSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2Zvcm0nKTtcbiAgICAgICAgZm9ybS5zZXRBdHRyaWJ1dGUoJ2lkJywgJ2NvbXBhbnktZm9ybScpO1xuICAgICAgICBmb3JtLmlubmVySFRNTCA9IFwiXFxuICAgIDxpbWcgc3JjPVxcXCJodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vcmV2dWZsb3ctYXNzZXRzL3JldnVmbG93X2xvZ29faW52ZXJzZWQucG5nXFxcIiB3aWR0aD1cXFwiNTAlXFxcIiBhbHQ9XFxcInJldnVmbG93X2xvZ29cXFwiLz5cXG4gICAgPGJyPlxcbiAgICA8bGFiZWwgZm9yPVxcXCJjb21wYW55XFxcIj5Db21wYW55IG5hbWU8L2xhYmVsPlxcbiAgICA8aW5wdXQgdHlwZT1cXFwidGV4dFxcXCIgaWQ9XFxcImNvbXBhbnlcXFwiIHZhbHVlPVxcXCJcIi5jb25jYXQoY29tcGFueSwgXCJcXFwiPlxcbiAgICA8YnV0dG9uPlN1Ym1pdDwvYnV0dG9uPlxcbiAgICBcIik7XG4gICAgICAgIGZvcm0uYWRkRXZlbnRMaXN0ZW5lcihcInN1Ym1pdFwiLCBzZXR0aW5nc1N1Ym1pdEhhbmRsZXIpO1xuICAgICAgICByZXR1cm4gZm9ybTtcbiAgICB9XG59O1xuZnVuY3Rpb24gc2V0dGluZ3NTdWJtaXRIYW5kbGVyKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgdmFyIGNvbXBhbnlJbnB1dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY29tcGFueVwiKTtcbiAgICB2YXIgY29tcGFueSA9IGNvbXBhbnlJbnB1dC52YWx1ZTtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJldnVmbG93LWNvbXBhbnlcIiwgY29tcGFueSk7XG4gICAgdmFyIGNyb3NzSWNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3Jvc3NcIik7XG4gICAgdmFyIHNldHRpbmdzSWNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2V0dGluZ3NcIik7XG4gICAgY3Jvc3NJY29uLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICBzZXR0aW5nc0ljb24uc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgICBUZW1wbGF0ZXNHcmlkXzEuVGVtcGxhdGVzR3JpZC5zaG93KCk7XG59XG4iLCJcInVzZSBzdHJpY3RcIjtcbnZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xuICAgIGZ1bmN0aW9uIGFkb3B0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFAgPyB2YWx1ZSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGFkb3B0KHJlc3VsdC52YWx1ZSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XG4gICAgfSk7XG59O1xudmFyIF9fZ2VuZXJhdG9yID0gKHRoaXMgJiYgdGhpcy5fX2dlbmVyYXRvcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIGJvZHkpIHtcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xuICAgIGZ1bmN0aW9uIHZlcmIobikgeyByZXR1cm4gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHN0ZXAoW24sIHZdKTsgfTsgfVxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xuICAgICAgICB3aGlsZSAoZyAmJiAoZyA9IDAsIG9wWzBdICYmIChfID0gMCkpLCBfKSB0cnkge1xuICAgICAgICAgICAgaWYgKGYgPSAxLCB5ICYmICh0ID0gb3BbMF0gJiAyID8geVtcInJldHVyblwiXSA6IG9wWzBdID8geVtcInRocm93XCJdIHx8ICgodCA9IHlbXCJyZXR1cm5cIl0pICYmIHQuY2FsbCh5KSwgMCkgOiB5Lm5leHQpICYmICEodCA9IHQuY2FsbCh5LCBvcFsxXSkpLmRvbmUpIHJldHVybiB0O1xuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XG4gICAgfVxufTtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcbmV4cG9ydHMuU3RyaXBlRm9ybSA9IHZvaWQgMDtcbnZhciB1dGlsc18xID0gcmVxdWlyZShcIi4uL3V0aWxzXCIpO1xudmFyIFJldnVmbG93XzEgPSByZXF1aXJlKFwiLi4vYXBpLWhlbHBlcnMvUmV2dWZsb3dcIik7XG52YXIgQ29tcGFueUZvcm1fMSA9IHJlcXVpcmUoXCIuL0NvbXBhbnlGb3JtXCIpO1xuZXhwb3J0cy5TdHJpcGVGb3JtID0ge1xuICAgIGNyZWF0ZUZvcm06IGZ1bmN0aW9uICh0ZW1wbGF0ZUlkKSB7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW1hZ2VzLXJvd1wiKS5yZW1vdmUoKTtcbiAgICAgICAgdmFyIHN0cmlwZUZvcm0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdmb3JtJyk7XG4gICAgICAgIHN0cmlwZUZvcm0uc2V0QXR0cmlidXRlKFwiaWRcIiwgXCJzdHJpcGUtZm9ybVwiKTtcbiAgICAgICAgc3RyaXBlRm9ybS5pbm5lckhUTUwgPSBcIlxcbiAgICAgICAgPGltZyBzcmM9XFxcImh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9yZXZ1Zmxvdy1hc3NldHMvcmV2dWZsb3dfbG9nb19pbnZlcnNlZC5wbmdcXFwiIHdpZHRoPVxcXCIxMDAlXFxcIiBhbHQ9XFxcInJldnVmbG93X2xvZ29cXFwiLz5cXG4gICAgICAgIDxkaXYgc3R5bGU9XFxcImZvbnQtc2l6ZTogMTJweFxcXCI+Tm90IHN1c2NyaWJlZCB5ZXQgPyBWaXNpdCA8YSBocmVmPVxcXCJodHRwczovL3d3dy5yZXZ1Zmxvdy5uZXQvXFxcIiB0YXJnZXQ9XFxcIl9ibGFua1xcXCIgc3R5bGU9XFxcImNvbG9yOiAjMTI4MEVFXFxcIj5vdXIgd2Vic2l0ZTwvYT4gdG8gZW5qb3kgeW91ciAzMC1kYXkgZnJlZSB0cmlhbCBub3cgITwvZGl2PlxcbiAgICAgICAgPGJyPlxcbiAgICAgICAgPGxhYmVsIHN0eWxlPVxcXCJmb250LXNpemU6IDE0cHhcXFwiIGZvcj1cXFwic3RyaXBlLWVtYWlsXFxcIiBjbGFzcz1cXFwidHMtc21hbGxcXFwiPkVudGVyIHlvdXIgU3RyaXBlIHN1YnNjcmlwdGlvbiBlLW1haWw8L2xhYmVsPlxcbiAgICAgICAgPGlucHV0IHR5cGU9XFxcImVtYWlsXFxcIiBpZD1cXFwic3RyaXBlLWVtYWlsXFxcIj5cXG4gICAgICAgIDxkaXYgc3R5bGU9XFxcImZvbnQtc2l6ZTogMTRweFxcXCIgaWQ9XFxcIm1hdGNoaW5nLWVtYWlsc1xcXCI+XFxuICAgICAgICBUaGUgZS1tYWlsIGFkZHJlc3MgeW91IGVudGVyIGhlcmUgbXVzdCBtYXRjaCB3aXRoIHRoZSBlLW1haWwgeW91IHVzZWQgdG8gcGF5IGZvciBSZXZ1RmxvdyBvbiBTdHJpcGUuXFxuICAgICAgICA8L2Rpdj5cXG4gICAgICAgIDxidXR0b24+U3VibWl0PC9idXR0b24+XFxuICAgIFwiO1xuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZChzdHJpcGVGb3JtKTtcbiAgICAgICAgc3RyaXBlRm9ybS5hZGRFdmVudExpc3RlbmVyKFwic3VibWl0XCIsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgdmFyIGVtYWlsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzdHJpcGUtZW1haWxcIikudmFsdWU7XG4gICAgICAgICAgICBwcm9jZXNzU3Vic2NyaXB0aW9uKGVtYWlsLCB0ZW1wbGF0ZUlkLCBldmVudCk7XG4gICAgICAgIH0pO1xuICAgIH1cbn07XG5mdW5jdGlvbiBwcm9jZXNzU3Vic2NyaXB0aW9uKGVtYWlsLCB0ZW1wbGF0ZUlkLCBldmVudCkge1xuICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHNpdGVJbmZvLCB1c2VySGFzQWNjZXNzLCB1c2VySGFzU3Vic2NyaXB0aW9uLCB1c2VySGFzTWV0YWRhdGEsIGNvbXBhbnlGb3JtLCBzdHJpcGVGb3JtLCBjb21wYW55TmFtZSwgY29tcGFueUZvcm0sIHN0cmlwZUZvcm0sIGVfMTtcbiAgICAgICAgcmV0dXJuIF9fZ2VuZXJhdG9yKHRoaXMsIGZ1bmN0aW9uIChfYSkge1xuICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50KVxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF1dGlsc18xLkNvbW1vbnMudmFsaWRhdGVFbWFpbChlbWFpbCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdlYmZsb3cubm90aWZ5KHsgdHlwZTogJ0Vycm9yJywgbWVzc2FnZTogJ1RoZSBlLW1haWwgZm9ybWF0IGlzIGludmFsaWQuJyB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbMiAvKnJldHVybiovXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCB3ZWJmbG93LmdldFNpdGVJbmZvKCldO1xuICAgICAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICAgICAgc2l0ZUluZm8gPSBfYS5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbNCAvKnlpZWxkKi8sIFJldnVmbG93XzEuUmV2dWZsb3cuaGFzQWNjZXNzKGVtYWlsLCBzaXRlSW5mby5zaXRlSWQpXTtcbiAgICAgICAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICAgICAgICAgIHVzZXJIYXNBY2Nlc3MgPSBfYS5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbNCAvKnlpZWxkKi8sIFJldnVmbG93XzEuUmV2dWZsb3cuaGFzU3Vic2NyaXB0aW9uKGVtYWlsKV07XG4gICAgICAgICAgICAgICAgY2FzZSAzOlxuICAgICAgICAgICAgICAgICAgICB1c2VySGFzU3Vic2NyaXB0aW9uID0gX2Euc2VudCgpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoISF1c2VySGFzU3Vic2NyaXB0aW9uKSByZXR1cm4gWzMgLypicmVhayovLCA1XTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgd2ViZmxvdy5ub3RpZnkoeyB0eXBlOiAnRXJyb3InLCBtZXNzYWdlOiAnVGhlIGdpdmVuIGUtbWFpbCBkb2VzIG5vdCBoYXZlIGFueSBhY3RpdmUgc3Vic2NyaXB0aW9uLicgfSldO1xuICAgICAgICAgICAgICAgIGNhc2UgNDpcbiAgICAgICAgICAgICAgICAgICAgX2Euc2VudCgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qL107XG4gICAgICAgICAgICAgICAgY2FzZSA1OlxuICAgICAgICAgICAgICAgICAgICBpZiAoISF1c2VySGFzQWNjZXNzKSByZXR1cm4gWzMgLypicmVhayovLCAxMF07XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbNCAvKnlpZWxkKi8sIFJldnVmbG93XzEuUmV2dWZsb3cuaGFzTWV0YWRhdGEoZW1haWwpXTtcbiAgICAgICAgICAgICAgICBjYXNlIDY6XG4gICAgICAgICAgICAgICAgICAgIHVzZXJIYXNNZXRhZGF0YSA9IF9hLnNlbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEhdXNlckhhc01ldGFkYXRhKSByZXR1cm4gWzMgLypicmVhayovLCA4XTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgUmV2dWZsb3dfMS5SZXZ1Zmxvdy51cGRhdGVVc2VyTWV0YWRhdGEoZW1haWwsIHNpdGVJbmZvLnNpdGVJZCldO1xuICAgICAgICAgICAgICAgIGNhc2UgNzpcbiAgICAgICAgICAgICAgICAgICAgX2Euc2VudCgpO1xuICAgICAgICAgICAgICAgICAgICBjb21wYW55Rm9ybSA9IENvbXBhbnlGb3JtXzEuQ29tcGFueUZvcm0uY3JlYXRlRm9ybSgpO1xuICAgICAgICAgICAgICAgICAgICBzdHJpcGVGb3JtID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzdHJpcGUtZm9ybVwiKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHN0cmlwZUZvcm0pXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHJpcGVGb3JtLnJlcGxhY2VXaXRoKGNvbXBhbnlGb3JtKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFszIC8qYnJlYWsqLywgOV07XG4gICAgICAgICAgICAgICAgY2FzZSA4OlxuICAgICAgICAgICAgICAgICAgICB3ZWJmbG93Lm5vdGlmeSh7IHR5cGU6ICdFcnJvcicsIG1lc3NhZ2U6ICdUaGUgZ2l2ZW4gZS1tYWlsIGRvZXMgbm90IGhhdmUgYWNjZXNzIHRvIHRoZSBleHRlbnNpb24gZm9yIHRoaXMgd2Vic2l0ZS4nIH0pO1xuICAgICAgICAgICAgICAgICAgICBfYS5sYWJlbCA9IDk7XG4gICAgICAgICAgICAgICAgY2FzZSA5OiByZXR1cm4gWzIgLypyZXR1cm4qL107XG4gICAgICAgICAgICAgICAgY2FzZSAxMDpcbiAgICAgICAgICAgICAgICAgICAgX2EudHJ5cy5wdXNoKFsxMCwgMTQsICwgMTVdKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInJldnVmbG93LWVtYWlsXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJldnVmbG93LWVtYWlsXCIsIGVtYWlsKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjb21wYW55TmFtZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmV2dWZsb3ctY29tcGFueVwiKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFjb21wYW55TmFtZSkgcmV0dXJuIFszIC8qYnJlYWsqLywgMTJdO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBDb21wYW55Rm9ybV8xLkNvbXBhbnlGb3JtLmhhbmRsZUZvcm0odGVtcGxhdGVJZCldO1xuICAgICAgICAgICAgICAgIGNhc2UgMTE6XG4gICAgICAgICAgICAgICAgICAgIF9hLnNlbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFszIC8qYnJlYWsqLywgMTNdO1xuICAgICAgICAgICAgICAgIGNhc2UgMTI6XG4gICAgICAgICAgICAgICAgICAgIGlmICh1c2VySGFzQWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb21wYW55Rm9ybSA9IENvbXBhbnlGb3JtXzEuQ29tcGFueUZvcm0uY3JlYXRlRm9ybSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RyaXBlRm9ybSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic3RyaXBlLWZvcm1cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3RyaXBlRm9ybSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHJpcGVGb3JtLnJlcGxhY2VXaXRoKGNvbXBhbnlGb3JtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY29tcGFueS1mb3JtXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJzdWJtaXRcIiwgZnVuY3Rpb24gKGV2ZW50KSB7IHJldHVybiBDb21wYW55Rm9ybV8xLkNvbXBhbnlGb3JtLmhhbmRsZUZvcm0odGVtcGxhdGVJZCwgZXZlbnQpOyB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4cG9ydHMuU3RyaXBlRm9ybS5jcmVhdGVGb3JtKHRlbXBsYXRlSWQpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIF9hLmxhYmVsID0gMTM7XG4gICAgICAgICAgICAgICAgY2FzZSAxMzogcmV0dXJuIFszIC8qYnJlYWsqLywgMTVdO1xuICAgICAgICAgICAgICAgIGNhc2UgMTQ6XG4gICAgICAgICAgICAgICAgICAgIGVfMSA9IF9hLnNlbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBmZXRjaCBzdWJzY3JpcHRpb24gc3RhdHVzOlwiLCBlXzEpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzMgLypicmVhayovLCAxNV07XG4gICAgICAgICAgICAgICAgY2FzZSAxNTogcmV0dXJuIFsyIC8qcmV0dXJuKi9dO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcbn1cbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XG4gICAgZnVuY3Rpb24gYWRvcHQodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUCA/IHZhbHVlIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogYWRvcHQocmVzdWx0LnZhbHVlKS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcbiAgICB9KTtcbn07XG52YXIgX19nZW5lcmF0b3IgPSAodGhpcyAmJiB0aGlzLl9fZ2VuZXJhdG9yKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgYm9keSkge1xuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XG4gICAgICAgIHdoaWxlIChnICYmIChnID0gMCwgb3BbMF0gJiYgKF8gPSAwKSksIF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xuZXhwb3J0cy5UZW1wbGF0ZXNHcmlkID0gdm9pZCAwO1xudmFyIHV0aWxzXzEgPSByZXF1aXJlKFwiLi4vdXRpbHNcIik7XG52YXIgU3RyaXBlRm9ybV8xID0gcmVxdWlyZShcIi4vU3RyaXBlRm9ybVwiKTtcbnZhciBDb21wYW55Rm9ybV8xID0gcmVxdWlyZShcIi4vQ29tcGFueUZvcm1cIik7XG52YXIgQVZBSUxBQkxFX1RFTVBMQVRFUyA9IFsyLCAzXTtcbmV4cG9ydHMuVGVtcGxhdGVzR3JpZCA9IHtcbiAgICBzaG93OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBfYTtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHJlc3BvbnNlLCBpdGVtcywgdGVtcGxhdGVzLCByb3csIGxlZnRDb2x1bW4sIHJpZ2h0Q29sdW1uLCBpc0VtYWlsU3RvcmVkQW5kVmFsaWQsIGltYWdlcztcbiAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2IpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKF9iLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlbW92ZUVsZW1lbnRCeUlkKFwiY29tcGFueS1mb3JtXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlRWxlbWVudEJ5SWQoXCJzdHJpcGUtZm9ybVwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbNCAvKnlpZWxkKi8sIGZldGNoKCdodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vc3RvcmFnZS92MS9iL3JldnVmbG93LWFzc2V0cy9vP3ByZWZpeD10ZW1wbGF0ZXMvJyldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZSA9IF9iLnNlbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbNCAvKnlpZWxkKi8sIHJlc3BvbnNlLmpzb24oKV07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMjpcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW1zID0gKF9iLnNlbnQoKSkuaXRlbXM7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZXMgPSBpdGVtcy5zbGljZSgxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdyA9IGNyZWF0ZURpdldpdGhBdHRyaWJ1dGVzQW5kU3R5bGVzKHsgaWQ6IFwiaW1hZ2VzLXJvd1wiIH0sIHsgZGlzcGxheTogXCJmbGV4XCIgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0Q29sdW1uID0gY3JlYXRlRGl2V2l0aEF0dHJpYnV0ZXNBbmRTdHlsZXMoe30sIHsgZmxleDogXCI1MCVcIiwgcGFkZGluZzogXCI1cHhcIiB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0Q29sdW1uID0gY3JlYXRlRGl2V2l0aEF0dHJpYnV0ZXNBbmRTdHlsZXMoe30sIHsgZmxleDogXCI1MCVcIiwgcGFkZGluZzogXCI1cHhcIiB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRW1haWxTdG9yZWRBbmRWYWxpZCA9IHV0aWxzXzEuQ29tbW9ucy52YWxpZGF0ZUVtYWlsKChfYSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmV2dWZsb3ctZW1haWxcIikpICE9PSBudWxsICYmIF9hICE9PSB2b2lkIDAgPyBfYSA6IFwiXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2VzID0gY3JlYXRlSW1hZ2VzKHRlbXBsYXRlcywgISFpc0VtYWlsU3RvcmVkQW5kVmFsaWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY3JlYXRlIHRoZSB0d28gdGVtcGxhdGVzIHJvd3NcbiAgICAgICAgICAgICAgICAgICAgICAgIGFwcGVuZENoaWxkcmVuVG9FbGVtZW50KGxlZnRDb2x1bW4sIGltYWdlcy5zbGljZSgwLCBpbWFnZXMubGVuZ3RoIC8gMikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgYXBwZW5kQ2hpbGRyZW5Ub0VsZW1lbnQocmlnaHRDb2x1bW4sIGltYWdlcy5zbGljZShpbWFnZXMubGVuZ3RoIC8gMikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcm93LmFwcGVuZChsZWZ0Q29sdW1uLCByaWdodENvbHVtbik7XG4gICAgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZChyb3cpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi9dO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59O1xuZnVuY3Rpb24gcmVtb3ZlRWxlbWVudEJ5SWQoaWQpIHtcbiAgICB2YXIgZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKTtcbiAgICBpZiAoZWxlbWVudClcbiAgICAgICAgZWxlbWVudC5yZW1vdmUoKTtcbn1cbmZ1bmN0aW9uIGNyZWF0ZURpdldpdGhBdHRyaWJ1dGVzQW5kU3R5bGVzKGF0dHJpYnV0ZXMsIHN0eWxlcykge1xuICAgIHZhciBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIGZvciAodmFyIF9pID0gMCwgX2EgPSBPYmplY3QuZW50cmllcyhhdHRyaWJ1dGVzKTsgX2kgPCBfYS5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgdmFyIF9iID0gX2FbX2ldLCBrZXkgPSBfYlswXSwgdmFsdWUgPSBfYlsxXTtcbiAgICAgICAgZGl2LnNldEF0dHJpYnV0ZShrZXksIHZhbHVlKTtcbiAgICB9XG4gICAgT2JqZWN0LmFzc2lnbihkaXYuc3R5bGUsIHN0eWxlcyk7XG4gICAgcmV0dXJuIGRpdjtcbn1cbmZ1bmN0aW9uIGNyZWF0ZUltYWdlcyh0ZW1wbGF0ZXMsIGlzRW1haWxTdG9yZWRBbmRWYWxpZCkge1xuICAgIHJldHVybiB0ZW1wbGF0ZXMubWFwKGZ1bmN0aW9uICh0ZW1wbGF0ZSwgaW5kZXgpIHtcbiAgICAgICAgdmFyIHRlbXBsYXRlSWQgPSBcIlwiLmNvbmNhdChpbmRleCArIDEpO1xuICAgICAgICB2YXIgaW1hZ2UgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW1nXCIpO1xuICAgICAgICBpbWFnZS5zZXRBdHRyaWJ1dGUoXCJzcmNcIiwgXCJodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vcmV2dWZsb3ctYXNzZXRzL1wiLmNvbmNhdCh0ZW1wbGF0ZS5uYW1lKSk7XG4gICAgICAgIGltYWdlLnNldEF0dHJpYnV0ZShcInRlbXBsYXRlLWlkXCIsIHRlbXBsYXRlSWQpO1xuICAgICAgICBpbWFnZS5zZXRBdHRyaWJ1dGUoXCJ3aWR0aFwiLCBcIjEwMCVcIik7XG4gICAgICAgIGlmICghKEFWQUlMQUJMRV9URU1QTEFURVMuaW5kZXhPZihpbmRleCArIDEpID49IDApKSB7XG4gICAgICAgICAgICBpbWFnZS5zdHlsZS5maWx0ZXIgPSBcImJsdXIoMC4xNXJlbSlcIjtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGltYWdlLnN0eWxlLmN1cnNvciA9IFwicG9pbnRlclwiO1xuICAgICAgICAgICAgaW1hZ2Uub25jbGljayA9IGNyZWF0ZUltYWdlQ2xpY2tIYW5kbGVyKHRlbXBsYXRlSWQsIGlzRW1haWxTdG9yZWRBbmRWYWxpZCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGltYWdlO1xuICAgIH0pO1xufVxuZnVuY3Rpb24gY3JlYXRlSW1hZ2VDbGlja0hhbmRsZXIodGVtcGxhdGVJZCwgaXNFbWFpbFN0b3JlZEFuZFZhbGlkKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCFpc0VtYWlsU3RvcmVkQW5kVmFsaWQpIHtcbiAgICAgICAgICAgIFN0cmlwZUZvcm1fMS5TdHJpcGVGb3JtLmNyZWF0ZUZvcm0odGVtcGxhdGVJZCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBpZiAoIWxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmV2dWZsb3ctY29tcGFueVwiKSkge1xuICAgICAgICAgICAgICAgIHZhciBjb21wYW55Rm9ybSA9IENvbXBhbnlGb3JtXzEuQ29tcGFueUZvcm0uY3JlYXRlRm9ybSgpO1xuICAgICAgICAgICAgICAgIHJlcGxhY2VFbGVtZW50QnlJZChcImltYWdlcy1yb3dcIiwgY29tcGFueUZvcm0pO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY29tcGFueS1mb3JtXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJzdWJtaXRcIiwgZnVuY3Rpb24gKGV2ZW50KSB7IHJldHVybiBDb21wYW55Rm9ybV8xLkNvbXBhbnlGb3JtLmhhbmRsZUZvcm0odGVtcGxhdGVJZCwgZXZlbnQpOyB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIENvbXBhbnlGb3JtXzEuQ29tcGFueUZvcm0uaGFuZGxlRm9ybSh0ZW1wbGF0ZUlkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG59XG5mdW5jdGlvbiByZXBsYWNlRWxlbWVudEJ5SWQoaWQsIG5ld0VsZW1lbnQpIHtcbiAgICB2YXIgb2xkRWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKTtcbiAgICBpZiAob2xkRWxlbWVudClcbiAgICAgICAgb2xkRWxlbWVudC5yZXBsYWNlV2l0aChuZXdFbGVtZW50KTtcbn1cbmZ1bmN0aW9uIGFwcGVuZENoaWxkcmVuVG9FbGVtZW50KGVsZW1lbnQsIGNoaWxkcmVuKSB7XG4gICAgY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpIHsgcmV0dXJuIGVsZW1lbnQuYXBwZW5kKGNoaWxkKTsgfSk7XG59XG4iLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG52YXIgVGVtcGxhdGVzR3JpZF8xID0gcmVxdWlyZShcIi4vdmlld3MvVGVtcGxhdGVzR3JpZFwiKTtcbnZhciBTZXR0aW5nc0Zvcm1fMSA9IHJlcXVpcmUoXCIuL3ZpZXdzL1NldHRpbmdzRm9ybVwiKTtcbi8vIGluaXRpYWxpemVcbndlYmZsb3cuc2V0RXh0ZW5zaW9uU2l6ZShcImNvbWZvcnRhYmxlXCIpO1xuVGVtcGxhdGVzR3JpZF8xLlRlbXBsYXRlc0dyaWQuc2hvdygpO1xuLy8gRXZlbnQgSGFuZGxlcnNcbndpbmRvdy5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHNldHRpbmdzSWNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2V0dGluZ3NcIik7XG4gICAgdmFyIGNyb3NzSWNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3Jvc3NcIik7XG4gICAgc2V0dGluZ3NJY29uLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBzZXR0aW5nc0NsaWNrSGFuZGxlcik7XG4gICAgY3Jvc3NJY29uLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBjcm9zc0NsaWNrSGFuZGxlcik7XG59O1xuZnVuY3Rpb24gc2V0dGluZ3NDbGlja0hhbmRsZXIoKSB7XG4gICAgdmFyIHNldHRpbmdzSWNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2V0dGluZ3NcIik7XG4gICAgdmFyIGNyb3NzSWNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3Jvc3NcIik7XG4gICAgc2V0dGluZ3NJY29uLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICBjcm9zc0ljb24uc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImltYWdlcy1yb3dcIikucmVtb3ZlKCk7XG4gICAgdmFyIHNldHRpbmdzRm9ybSA9IFNldHRpbmdzRm9ybV8xLlNldHRpbmdzRm9ybS5jcmVhdGVGb3JtKCk7XG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmQoc2V0dGluZ3NGb3JtKTtcbn1cbmZ1bmN0aW9uIGNyb3NzQ2xpY2tIYW5kbGVyKCkge1xuICAgIHZhciBjcm9zc0ljb24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNyb3NzXCIpO1xuICAgIHZhciBzZXR0aW5nc0ljb24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNldHRpbmdzXCIpO1xuICAgIGNyb3NzSWNvbi5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgc2V0dGluZ3NJY29uLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XG4gICAgVGVtcGxhdGVzR3JpZF8xLlRlbXBsYXRlc0dyaWQuc2hvdygpO1xufVxuIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9